# Future work

## Planned

 - handle numbers (soon)
 - more help in DataNode with navigating tress (as soon as usage makes it clear what is needed)
 - make dataclasses.asdict more visible, possibly as a class function (probably soon)
 - more test cases from the game itself (everytime errors or pitfalls are encountered)

## Maybe

 - Classes aware of the internal structure of game data (Ship, Outfit, ...)
 - Once we have that, ability to load and create ships with outfits
 - Once we have that, replicate game internal math
   - Speed, acceleration from mass, friction and engines
   - Thermal load
   - Shield reload time
   - Energy balance
   - Crew required
